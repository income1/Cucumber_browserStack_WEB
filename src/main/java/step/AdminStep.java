package step;

import com.epam.reportportal.testng.ReportPortalTestNGListener;
import core.BaseTest;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.testng.annotations.Listeners;
import page.AdminPage;


public class AdminStep extends BaseTest {
    AdminPage adminAccountPage = new AdminPage(getDriver());

    @When("Click to the Customers on the left menu")
    public void clickToTheCustomersOnTheLeftMenu() {
        adminAccountPage.clickCustomersBtn();
    }

    @And("Click to the All Customers")
    public void clickToTheAllCustomers() {
        adminAccountPage.clickAllCustomerBtn();
    }

    @When("Click to the Catalog on the left menu.")
    public void clickToTheCatalogOnTheLeftMenu() {
        adminAccountPage.clickCatalogBtn();
    }

    @And("Click to the Products")
    public void clickToTheProducts() {
        adminAccountPage.clickProductsBtn();
    }


}
