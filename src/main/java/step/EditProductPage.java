package step;

import com.epam.reportportal.testng.ReportPortalTestNGListener;
import core.BaseTest;
import io.cucumber.java.en.Then;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import page.EditProductsPage;

public class EditProductPage extends BaseTest {
    EditProductsPage editProductPage = new EditProductsPage(getDriver());

    @Then("The edit product page display")
    public void theEditProductPageDisplay() {
        Assert.assertEquals(editProductPage.getValueProductName(), "Joust Duffle Bag");
        Assert.assertEquals(editProductPage.getValueProductPrice(), "34.00");
        Assert.assertEquals(editProductPage.getValueProductAttribute(), "Bag");
    }
}
