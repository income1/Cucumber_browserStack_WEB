package step;

import com.epam.reportportal.testng.ReportPortalTestNGListener;
import core.BaseTest;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import page.EditCustomerPage;

public class EditCustomerStep extends BaseTest {
    EditCustomerPage editCustomerPage = new EditCustomerPage(getDriver());

    @And("Click to the Account Information")
    public void clickToTheAccountInformation() {
        editCustomerPage.clickAccountInformationBtn();
    }

    @And("The first name and lastname should be: {string}")
    public void theFirstNameAndLastnameShouldBe(String key) {
        Assert.assertEquals(editCustomerPage.getFirstName(), editCustomerPage.getFirstNameFromKey(key));
        Assert.assertEquals(editCustomerPage.getLastName(), editCustomerPage.getSecondNameFromKey(key));
    }

    @Then("The Edit customer page display")
    public void theEditCustomerPageDisplay() {
        Assert.assertTrue(editCustomerPage.checkSaveCustomerBtnIsDisplay());
    }
}
