package step;

import core.BaseTest;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

public class Hooks {
    BaseTest base = new BaseTest();

    @Before
    public void beforeSuite() throws MalformedURLException {
        base.openBrowser();
    }

    @After
    public void afterSuite(Scenario scenario) throws IOException {
        //validate if scenario has failed then Screenshot
        if (scenario.isFailed()) {
            File image = ((TakesScreenshot) base.getDriver()).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(image, new File("E:\\Fresher Test\\DemoMagento\\src\\main\\java\\image\\" + scenario.getName() + "Error.png"));
        }
        System.out.println("Stop Driver: " + scenario.getName());
        base.getDriver().quit();
    }
}
