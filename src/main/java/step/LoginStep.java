package step;

import com.epam.reportportal.testng.ReportPortalTestNGListener;
import core.BaseTest;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import org.testng.annotations.Listeners;
import page.LoginPage;

import java.util.List;
import java.util.Map;


public class LoginStep extends BaseTest {
    LoginPage loginPage = new LoginPage(getDriver());

    @Given("User open the browser for website url and login with username and password")
    public void userHasOpenedMagentoWebsiteAndLogin(DataTable table) {
        List<Map<String, String>> data = table.asMaps(String.class, String.class);
        loginPage.navigateToURL(data.get(0).get("url"));
        loginPage.login(data.get(0).get("username"), data.get(0).get("password"));
    }


}
