package step;

import com.epam.reportportal.testng.ReportPortalTestNGListener;
import core.BaseTest;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import page.AllCustomerPage;

public class AllCustomerStep extends BaseTest {
    AllCustomerPage allCustomerPage = new AllCustomerPage(getDriver());

    @And("Click Add New Customer")
    public void clickAddNewCustomer() {
        allCustomerPage.clickAddNewCustomerBtn();
    }

    @Then("The message should be displayed: You saved the customer.")
    public void theMessageShouldBeDisplayedYouSavedTheCustomer() {
        Assert.assertEquals(allCustomerPage.getTextOfMessage(), "You saved the customer.");
    }

    @And("Input search key {string}")
    public void inputSearchKey(String key) {
        allCustomerPage.sendSearchKeyText(key);
    }

    @And("Click edit")
    public void clickEdit() throws InterruptedException {
        allCustomerPage.clickEditBtn();
    }


}
