package step;

import com.epam.reportportal.testng.ReportPortalTestNGListener;
import core.BaseTest;
import io.cucumber.java.en.And;
import org.testng.annotations.Listeners;
import page.AddNewCustomerPage;


public class AddNewCustomerStep extends BaseTest {
    AddNewCustomerPage addNewCustomerPage = new AddNewCustomerPage(getDriver());

    @And("Input the requite field")
    public void inputTheRequiteField() {
        addNewCustomerPage.sendText(addNewCustomerPage.getFirstNameText(), "huy");
        addNewCustomerPage.sendText(addNewCustomerPage.getLastNameText(), "le");
        addNewCustomerPage.sendText(addNewCustomerPage.getEmailText(), "huylq@gmail.com");
    }

    @And("Click to the Save Customer")
    public void clickToTheSaveCustomer() {
        addNewCustomerPage.clickSaveNewCustomerBtn();
    }
}
