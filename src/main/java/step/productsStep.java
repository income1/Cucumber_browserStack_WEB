package step;

import com.epam.reportportal.testng.ReportPortalTestNGListener;
import core.BaseTest;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import page.ProductsPage;


public class productsStep extends BaseTest {
    ProductsPage productsPage = new ProductsPage(getDriver());

    @And("Input search key: {string} and click search")
    public void inputSearchKeyAndClickSearch(String key) {
        productsPage.sendSearchKeyText(key);
        productsPage.clickSearchBtn();
    }

    @Then("The search result display product with name: {string}")
    public void theSearchResultDisplayProductWithName(String key) {
        Assert.assertEquals(productsPage.getTextColumnNameOfTable(), key);
    }

    @Then("The product page should be displayed:Add product button, Search Key, Filters button, New view, Columns, Table listout product.")
    public void theProductPageShouldBeDisplayedAddProductButtonSearchKeyFiltersButtonNewViewColumnsTableListoutProduct() {
        Assert.assertTrue(productsPage.checkAddProductBtnIsDisplay());
        Assert.assertTrue(productsPage.checkSearchKeyTextIsDisplay());
        Assert.assertTrue(productsPage.checkFilterBtnIsDisplay());
        Assert.assertTrue(productsPage.checkTableProductIsDisplay());
    }

    @And("Click edit product")
    public void clickEditProduct() {
        productsPage.clickColumnActionOfTable();
    }


}
