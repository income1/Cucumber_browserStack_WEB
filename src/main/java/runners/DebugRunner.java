package runners;
import com.epam.reportportal.testng.ReportPortalTestNGListener;
import io.cucumber.testng.*;
import org.testng.TestNG;
import org.testng.annotations.*;
import org.testng.collections.Lists;

import java.util.Collections;
import java.util.List;

@CucumberOptions(
        features = "src/main/resources/features/MagentoDemo.feature",
        glue = {"step"},
        tags = ""
)
public class DebugRunner {
    private TestNGCucumberRunner testNGCucumberRunner;

    @BeforeClass(alwaysRun = true)
    public void setUpClass() {
        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
    }

    @Test(groups = "cucumber", description = "Runs Cucumber Scenarios", dataProvider = "scenario")
    public void scenario(PickleWrapper pickle, FeatureWrapper cucumberFeature) {
        testNGCucumberRunner.runScenario(pickle.getPickle());
    }

    @DataProvider
    public Object[][] scenario() {
        return testNGCucumberRunner.provideScenarios();
    }

    @AfterClass(alwaysRun = true)
    public void teardownClass() {
        testNGCucumberRunner.finish();
    }


}



