package core;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;


public class BaseTest {
    private static WebDriver driver;
    private static final int TIMEOUT = 10;
    private WebDriverWait webDriverWait;

    public static final String USERNAME = "quanghuy_kdr5FN";
    public static final String AUTOMATE_KEY = "nyw5r3yrB3P48s2fLq27";
    public static final String LINKURL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub.browserstack.com/wd/hub";

    public void openBrowser() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("browserName", "Chrome");
        capabilities.setCapability("browserVersion", "latest-beta");
        HashMap<String, Object> browserstackOptions = new HashMap<String, Object>();
        browserstackOptions.put("os", "Windows");
        browserstackOptions.put("osVersion", "10");
        browserstackOptions.put("resolution", "1024x768");
        browserstackOptions.put("local", "false");
        browserstackOptions.put("seleniumVersion", "3.14.0");
        capabilities.setCapability("bstack:options", browserstackOptions);

        driver = new RemoteWebDriver(new URL(LINKURL),capabilities);

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);

//        System.setProperty("webdriver.chrome.driver","E:\\Fresher Test\\auto test\\webdriver\\chromedriver.exe");
//        driver = new ChromeDriver();
//        driver.manage().window().maximize();
    }

    public void closeBrowser() {
        driver.quit();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebDriverWait getWebDriverWait() {
        webDriverWait = new WebDriverWait(driver, TIMEOUT);
        return webDriverWait;
    }
}