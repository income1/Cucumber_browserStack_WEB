package page;

import core.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginPage extends BasePage {
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "username")
    private WebElement userName;

    @FindBy(id = "login")
    private WebElement password;

    @FindBy(xpath = "//button[@class='action-login action-primary']")
    private WebElement btnSignIn;

    @FindBy(xpath = "//a[@class='action-forgotpassword']")
    private WebElement forgotPassword;

    public WebElement getUserName() {
        return userName;
    }

    public WebElement getPassword() {
        return password;
    }

    public WebElement getBtnSignIn() {
        return btnSignIn;
    }

    public WebElement getForgotPassword() {
        return forgotPassword;
    }

    public boolean checkUserNameIsDisplay() {
        return userName.isDisplayed();
    }

    public boolean checkPasswordIsDisplay() {
        return password.isDisplayed();
    }

    public boolean checkBtnSignInIsDisplay() {
        return btnSignIn.isDisplayed();
    }

    public boolean checkForgotPasswordIsDisplay() {
        return forgotPassword.isDisplayed();
    }

    public void navigateToURL(String url) {
        getDriver().navigate().to(url);
    }

    public void login(String u, String p) {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(userName));
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(userName));
        userName.sendKeys(u);
        password.sendKeys(p);
        btnSignIn.click();
    }

}
