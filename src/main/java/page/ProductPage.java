package page;

import core.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class ProductPage extends BasePage {
    public ProductPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "add")
    private WebElement addNewBtn;

    @FindBy(xpath = "//span/ancestor::button[@title='Search']")
    private WebElement searchBtn;

    @FindBy(xpath = "//button[@title='Reset Filter']")
    private WebElement resetFilterLink;

    @FindBy(xpath = "//tbody/tr")
    private List<WebElement> rowAttributeTable;

    @FindBy(xpath = "//tbody/tr[1]/td")
    private List<WebElement> colOfRowAttributeTable;

    @FindBy(xpath = "//div[@id='messages']//div[@data-ui-id='messages-message-success']")
    private WebElement messageLabel;


    public String getTextMessageLabel() {
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(messageLabel));
        return messageLabel.getText();
    }

    public void searchByAttributeCode(String key) {
        getWebDriverWait().until(ExpectedConditions.visibilityOfAllElements(colOfRowAttributeTable));
        getDriver().findElement(By.xpath("//table/thead//input[@name='attribute_code']")).sendKeys(key);
        searchBtn.click();
    }

    public boolean checkRowAfterSearch(int size, String attributeName) {
        if (getDriver().findElements(By.xpath("//table/tbody/tr")).size() != size) {
            System.out.println(getDriver().findElements(By.xpath("//table/tbody/tr")).size() + " size");
            System.out.println("ex:" + size);
            return false;
        }
        if (!getDriver().findElement(By.xpath("//table/tbody/tr/td[contains(@class,'col-frontend_label')]")).getText().trim().equals(attributeName)) {
            System.out.println(getDriver().findElement(By.xpath("//table/tbody/tr/td[contains(@class,'col-frontend_label')]")).getText().trim());
            System.out.println(attributeName);
            return false;
        }
        return true;
    }

    public void clickFirstProductTable() {
        getWebDriverWait().until(ExpectedConditions.visibilityOfAllElements(colOfRowAttributeTable));
        getDriver().findElement(By.xpath("//table/tbody/tr")).click();
    }

    public void clickAddNewBtn() {
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(addNewBtn));
        addNewBtn.click();
    }

    public boolean verìyProductPage() {
        if (!addNewBtn.isDisplayed()) {
            return false;
        }
        if (!searchBtn.isDisplayed()) {
            return false;
        }
        if (!resetFilterLink.isDisplayed()) {
            return false;
        }
        for (WebElement we : colOfRowAttributeTable) {
            if (!we.isDisplayed()) {
                return false;
            }
        }
        return rowAttributeTable.size() == 20;
    }

}
