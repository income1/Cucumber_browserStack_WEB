package page;

import core.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class EditProductsPage extends BasePage {
    public EditProductsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(name = "product[name]")
    private WebElement productName;

    @FindBy(name = "product[price]")
    private WebElement productPrice;

    @FindBy(xpath = "(//div[@class='admin__action-multiselect-text'])[1]")
    private WebElement attributeSet;

    public String getValueProductName() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(productName));
        return productName.getAttribute("value");
    }

    public String getValueProductPrice() {
        return productPrice.getAttribute("value");
    }

    public String getValueProductAttribute() {
        return attributeSet.getText();
    }
}
