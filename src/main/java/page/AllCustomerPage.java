package page;

import core.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class AllCustomerPage extends BasePage {

    public AllCustomerPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "add")
    private WebElement addNewCustomerBtn;

    public void clickAddNewCustomerBtn() {
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(addNewCustomerBtn));
        addNewCustomerBtn.click();
    }

    @FindBy(xpath = "//div[@data-ui-id='messages-message-success']")
    private WebElement message;

    public String getTextOfMessage() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(message));
        return message.getText();
    }

    @FindBy(xpath = "(//div[@class='data-grid-search-control-wrap'])[1]//input")
    private WebElement searchKeyText;

    public void sendSearchKeyText(String keySearch) {
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(searchKeyText));
        searchKeyText.clear();
        searchKeyText.sendKeys(keySearch);
    }

    @FindBy(xpath = "(//div[@class='admin__data-grid-wrap']/table/tbody)[2]/tr[position()=last()]//a[text()='Edit']")
    private WebElement editBtn;

    public void clickEditBtn() throws InterruptedException {
        Thread.sleep(3000);
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(editBtn));
        editBtn.click();
    }

}
