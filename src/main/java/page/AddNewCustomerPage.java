package page;

import core.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class AddNewCustomerPage extends BasePage {
    public AddNewCustomerPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@data-index='firstname']//input")
    private WebElement firstNameText;

    @FindBy(xpath = "//div[@data-index='lastname']//input")
    private WebElement lastNameText;

    @FindBy(xpath = "//div[@data-index='email']//input")
    private WebElement emailText;
    @FindBy(id = "save")
    private WebElement saveNewCustomerBtn;

    public WebElement getFirstNameText() {
        return firstNameText;
    }

    public WebElement getLastNameText() {
        return lastNameText;
    }

    public WebElement getEmailText() {
        return emailText;
    }

    public WebElement getSaveNewCustomerBtn() {
        return saveNewCustomerBtn;
    }

    public void clickSaveNewCustomerBtn() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(saveNewCustomerBtn));
        saveNewCustomerBtn.click();
    }

    public void sendText(WebElement webElement, String text) {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(webElement));
        webElement.sendKeys(text);
    }


}
