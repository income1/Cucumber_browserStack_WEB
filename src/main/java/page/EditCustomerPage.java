package page;

import core.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class EditCustomerPage extends BasePage {
    public EditCustomerPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@class='admin__page-nav']//span[text()='Account Information']")
    private WebElement accountInformationBtn;

    @FindBy(xpath = "//span[text()='Save Customer']/..")
    private WebElement saveCustomerBtn;

    @FindBy(name = "customer[firstname]")
    private WebElement firstNameText;

    @FindBy(name = "customer[lastname]")
    private WebElement lastNameText;

    public void clickAccountInformationBtn() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(accountInformationBtn));
        accountInformationBtn.click();
    }

    public boolean checkSaveCustomerBtnIsDisplay() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(saveCustomerBtn));
        return saveCustomerBtn.isDisplayed();
    }

    public String getFirstName() {
        return firstNameText.getAttribute("value");
    }

    public String getLastName() {
        return lastNameText.getAttribute("value");
    }

    public int getFirstSpaceIndexInString(String string) {
        char kyTu;
        for (int i = 0; i < string.length(); i++) {
            kyTu = string.charAt(i);
            if (Character.isSpaceChar(kyTu)) {
                return i;
            }
        }
        return string.length();
    }

    public String getFirstNameFromKey(String key) {
        int spaceIndex = getFirstSpaceIndexInString(key);
        return key.substring(0, spaceIndex);
    }

    public String getSecondNameFromKey(String key) {
        int spaceIndex = getFirstSpaceIndexInString(key);
        return key.substring(spaceIndex + 1);
    }
}
