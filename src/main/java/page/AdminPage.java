package page;

import core.BasePage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class AdminPage extends BasePage {
    public AdminPage(WebDriver driver) {
        super(driver);
    }


    @FindBy(className = "admin__menu")
    private WebElement adminMenu;

    @FindBy(xpath = "//nav[@class='admin__menu']//span[text()='Customers']/..")
    private WebElement customersBtn;

    @FindBy(xpath = "//nav[@class='admin__menu']//span[text()='All Customers']/..")
    private WebElement allCustomerBtn;

    @FindBy(xpath = "//nav[@class='admin__menu']//span[text()='Catalog']/..")
    private WebElement catalogBtn;

    @FindBy(xpath = "//ul[@role='menu']//span[text()='Products']/..")
    private WebElement productsBtn;

    @FindBy(id = "menu-magento-backend-stores")
    private WebElement storesBtn;

    @FindBy(xpath = "//li[@role='menu-item']//span[text()='Product']")
    private WebElement productBtn;

    @FindBy(id = "menu-magento-sales-sales")
    private WebElement salesBtn;

    @FindBy(xpath = "//li[@role='menu-item' and @class='item-sales-order    level-2']")
    private WebElement ordersBtn;

    @FindBy(id = "menu-magento-backend-marketing")
    private WebElement marketingBtn;

    @FindBy(xpath = "//*[@id=\"menu-magento-backend-marketing\"]//span[text()='Catalog Price Rule']")
    private WebElement priceRuleBtn;

    public WebElement getAdminMenu() {
        return adminMenu;
    }

    public WebElement getCustomersBtn() {
        return customersBtn;
    }

    public WebElement getAllCustomerBtn() {
        return allCustomerBtn;
    }

    public WebElement getCatalogBtn() {
        return catalogBtn;
    }

    public WebElement getProductsBtn() {
        return productsBtn;
    }

    public WebElement getStoresBtn() {
        return storesBtn;
    }

    public WebElement getProductBtn() {
        return productBtn;
    }

    public WebElement getSalesBtn() {
        return salesBtn;
    }

    public WebElement getOrdersBtn() {
        return ordersBtn;
    }

    public WebElement getMarketingBtn() {
        return marketingBtn;
    }

    public WebElement getPriceRuleBtn() {
        return priceRuleBtn;
    }

    public void clickBtn(WebElement webElement) {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(webElement));
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(webElement));
        webElement.click();
    }

    public void clickOrdersBtn() {
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(ordersBtn));
        ordersBtn.click();
    }

//    public void clickSalesBtnBtn() throws InterruptedException {
//        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(salesBtn));
////        ((JavascriptExecutor)getDriver()).executeScript("arguments[0].click();",salesBtn);
//        salesBtn.click();
//    }

    public void clickProductsBtn() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(productsBtn));
//        productsBtn.click();
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", productsBtn);
    }

    public void clickStoresBtn() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(storesBtn));
        storesBtn.click();
    }

    public void clickProductBtn() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(productBtn));
//        productsBtn.click();
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", productBtn);
    }

    public void clickCatalogBtn() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(catalogBtn));
        catalogBtn.click();
    }

    public void clickAllCustomerBtn() {
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(allCustomerBtn));
//         allCustomerBtn.click();
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", allCustomerBtn);

    }

    public void clickCustomersBtn() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(customersBtn));
        customersBtn.click();
    }

    public boolean checkAdminMenuIsDisplay() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(adminMenu));
        return adminMenu.isDisplayed();
    }


}
