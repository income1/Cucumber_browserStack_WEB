package page;

import core.BasePage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ProductsPage extends BasePage {
    public ProductsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "add_new_product-button")
    private WebElement addProductBtn;

    @FindBy(xpath = "(//input[@class='admin__control-text data-grid-search-control'])[1]")
    private WebElement searchKeyText;

    @FindBy(xpath = "//button[text()='Filters']")
    private WebElement FilterBtn;

    @FindBy(xpath = "(//table[@class='data-grid data-grid-draggable'])[2]")
    private WebElement tableProduct;

    @FindBy(xpath = "(//input[@class='admin__control-text data-grid-search-control'])[1]/following-sibling::button")
    private WebElement searchBtn;

    @FindBy(xpath = "//table/tbody/tr/td[4]/div")
    private WebElement columnNameOfTable;

    @FindBy(xpath = "//table/tbody/tr/td[14]/a")
    private WebElement columnActionOfTable;

    public boolean checkAddProductBtnIsDisplay() {
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(addProductBtn));
        return addProductBtn.isDisplayed();
    }

    public boolean checkSearchKeyTextIsDisplay() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(searchKeyText));
        return searchKeyText.isDisplayed();
    }

    public void sendSearchKeyText(String keySearch) {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(searchKeyText));
        searchKeyText.clear();
        searchKeyText.sendKeys(keySearch);
    }

    public boolean checkFilterBtnIsDisplay() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(FilterBtn));
        return FilterBtn.isDisplayed();
    }

    public boolean checkTableProductIsDisplay() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(tableProduct));
        return tableProduct.isDisplayed();
    }

    public void clickSearchBtn() {
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(searchBtn));
//        searchBtn.click();
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", searchBtn);
    }

    public String getTextColumnNameOfTable() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(columnNameOfTable));
        return columnNameOfTable.getText();
    }

    public void clickColumnActionOfTable() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(columnActionOfTable));
        //columnActionOfTable.click();
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", columnActionOfTable);
    }
}
