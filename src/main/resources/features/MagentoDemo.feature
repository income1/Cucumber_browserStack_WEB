Feature: Magento Test case

  Background:
    Given User open the browser for website url and login with username and password
      | url                                                                              | username | password     |
      | https://demoshops.splendid-internet.de/magento/demoshop-magento2-daily/demoadmin | demoshop | demoshop2022 |

  Scenario: Verify create new Customer
    When Click to the Customers on the left menu
    And Click to the All Customers
    And Click Add New Customer
    And Input the requite field
    And Click to the Save Customer
    Then The message should be displayed: You saved the customer.

  Scenario Outline: Verify Edit Customer
    When Click to the Customers on the left menu
    And Click to the All Customers
    And Input search key "<key>"
    And Click edit
    Then The Edit customer page display
    And Click to the Account Information
    And The first name and lastname should be: "<key>"
    Examples:
      | key    |
      | huy le |

  @123
  Scenario: Verify  Product page
    When Click to the Catalog on the left menu.
    And Click to the Products
    Then The product page should be displayed:Add product button, Search Key, Filters button, New view, Columns, Table listout product.

  Scenario Outline:Verify search Product page
    When Click to the Catalog on the left menu.
    And Click to the Products
    And Input search key: "<Key>" and click search
    Then The search result display product with name: "<Key>"
    Examples:
      | Key              |
      | Joust Duffle Bag |

  Scenario Outline:Verify edit Product page
    When Click to the Catalog on the left menu.
    And Click to the Products
    And Input search key: "<Key>" and click search
    And Click edit product
    Then The edit product page display
    Examples:
      | Key              |
      | Joust Duffle Bag |





